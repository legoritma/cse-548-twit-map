# Twitter Map Visualizer

## Getting Started

Download database from https://ia800501.us.archive.org/32/items/twitter_cikm_2010/twitter_cikm_2010.zip

Extract database to 

* t/twitter_cikm_2010/test_set_users.txt
* t/twitter_cikm_2010/test_set_tweets.txt

Run ```npm start```

### Prepare Database

call http://127.0.0.1:3000/import/tweets

It may take for a while

## Usage

Go to http://127.0.0.1:3000/
