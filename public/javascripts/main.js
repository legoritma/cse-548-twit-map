// This example adds a user-editable rectangle to the map.
// When the user changes the bounds of the rectangle,
// an info window pops up displaying the new bounds.

var rectangle;
var map;
var heatmap;
var activeInfoWindow;


var xhr;
var userShowed = {};
var markers = [];
var markerTarget;

var stopButton = $('#stop_button');

function stopAjax() {
    if (xhr) {
        xhr.abort();
        xhr = null;
    }
    stopButton.removeClass('have_query');
    stopButton.prop('disabled', true);
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 38.5452, lng: -102.5389},
        zoom: 4,
        streetViewControl: false,
        mapTypeControl: false,
        fullscreenControl: false
    });

    heatmap = new google.maps.visualization.HeatmapLayer({
        data: [],
        radius: 30,
        map: map
    });

    var bounds = {
        north: 40.599,
        south: 37.490,
        east: -95.443,
        west: -102.649
    };

    // Define the rectangle and set its editable property to true.
    rectangle = new google.maps.Rectangle({
        bounds: bounds,
        editable: true,
        draggable: true,
        fillOpacity: 0
    });

    rectangle.setMap(map);

    // Add an event listener on the rectangle.
    rectangle.addListener('bounds_changed', showNewRect);

    // Define an info window on the map.
    infoWindow = new google.maps.InfoWindow();
    showNewRect();
}

// Show the new coordinates for the rectangle in an info window.

/** @this {google.maps.Rectangle} */
function showNewRect(event) {
    stopAjax();
    var ne = rectangle.getBounds().getNorthEast();
    var sw = rectangle.getBounds().getSouthWest();
    $('#ne_lat').val(ne.lat());
    $('#ne_lng').val(ne.lng());
    $('#sw_lat').val(sw.lat());
    $('#sw_lng').val(sw.lng());
    $('#start').val(0);
    return;

    var contentString = '<b>Rectangle moved.</b><br>' +
        'New north-east corner: ' + ne.lat() + ', ' + ne.lng() + '<br>' +
        'New south-west corner: ' + sw.lat() + ', ' + sw.lng();

    // Set the info window's content and position.
    infoWindow.setContent(contentString);
    infoWindow.setPosition(ne);

    infoWindow.open(map);
}

function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}

function clearMarkers() {
    userShowed = {};
    for (let i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    markers.length = 0;
}

function getTweets() {
    xhr = $.ajax({
        url: '/search',
        type: 'post',
        data: getFormData($('#form')),
        success: function (response) {
            if (response.error) {
                console.log(response.error);
                return;
            }

            for (var i = 0; i < response.tweets.length; i++) {
                let tweet = response.tweets[i];
                let isNew = true;
                if (userShowed[tweet.userId] !== undefined) {
                    isNew = false;
                } else {
                    userShowed[tweet.userId] = {
                        location : new google.maps.LatLng(
                            tweet.location.coordinates[1],
                            tweet.location.coordinates[0]
                        ),
                        weight : 0,
                        tweets : []
                    };

                    let marker = new google.maps.Marker({
                        position: userShowed[tweet.userId].location,
                        map: markerTarget
                    });
                    let infowindow = new google.maps.InfoWindow({
                        content: "",
                        maxWidth: 550
                    });
                    let id = tweet.userId;
                    marker.addListener('click', function () {
                        if (activeInfoWindow) activeInfoWindow.close();
                        let counts = {};
                        userShowed[id].tweets
                            .forEach(function(x) { counts[x] = (counts[x] || 0)+1; });

                        let content = '<div id="myInfoWindow-content" style="max-height: 250px; overflow: auto;">';
                        for (let key in counts) {
                            if (counts.hasOwnProperty(key)) {
                                content +=
                                    (counts[key] > 1 ? '<span class="tweet_count">(' + counts[key] + ' times)</span> ' : "")
                                    + key + '<br />';
                            }
                        }
                        content += '</div>';
                        infowindow.setContent(content);
                        infowindow.open(map, marker);
                        activeInfoWindow = infowindow;
                    });
                    markers.push(marker);
                }
                userShowed[tweet.userId].weight++;
                userShowed[tweet.userId].tweets.push(
                    '<span class="tweet_date">' + tweet.date + '</span>'
                    + tweet.text
                );
            }
            if (response.tweets.length === response.limit) {
                $('#start').val(parseInt($('#start').val()) + parseInt($('#limit').val()));
                getTweets();
            } else {
                stopButton.removeClass('have_query');
                stopButton.prop('disabled', true);
                xhr = null;
            }
            
            heatmap.data.length = 0;
            for (let key in userShowed) {
                if (userShowed.hasOwnProperty(key)) {
                    heatmap.data.push({
                        location: userShowed[key].location,
                        weight: userShowed[key].weight
                    })
                }
            }
            heatmap.setData(heatmap.data);
        }
    });
    stopButton.prop('disabled', false);
    stopButton.addClass('have_query');
}

function toggleMarkers(show) {
    markerTarget = show ? map : null;
    for (let i = 0; i < markers.length; i++) {
        markers[i].setMap(markerTarget);
    }
}

$('#marker_toggle').change(function() {
    toggleMarkers(this.checked);
});


$('#heatmap_size').change(function() {
    heatmap.set('radius', this.value);
});

stopButton.click(stopAjax);

$('#form')
    .submit(function (e) {
        e.preventDefault();
        stopAjax();
        $('#start').val(0);
        clearMarkers();
        getTweets();
    });

stopAjax();

$('#map').height($('body').height() - $('#controller').outerHeight(true));
