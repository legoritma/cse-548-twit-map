const csv = require('csvtojson')
var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;


function connect_db(callback) {
    MongoClient.connect(
        'mongodb://127.0.0.1:27017/',
        function (err, dbServer) {
            if (err) throw err;
            var db = dbServer.db("twitter_map");

            db.createCollection("tweets",
                function (err, results) {
                    if (err) throw err;
                    console.log('collection created');

                    var collection = db.collection('tweets');

                    var p1 = collection.createIndex(
                        {location: '2dsphere'}
                    );
                    var p2 = collection.createIndex(
                        {tweetId: 1},
                        {unique: true}
                    );
                    Promise.all(
                        [p1, p2]
                    ).then(function (values) {
                        console.log("indexes created");
                        callback(collection);
                    })
                    .catch(function (e) {
                        console.log(e)
                    })
                }
            );
        }
    );
}

function import_tweets(collection) {
    var locations = {};
    csv({
        delimiter: '\t',
        noheader: true
    })
        .fromFile('t/twitter_cikm_2010/test_set_users.txt')

        .on('csv',(csvArr)=>{
            if (csvArr.length !== 2) {
                return;
            }

            var loc = {
                type: "Point",
                coordinates: csvArr[1].replace('UT: ', '').split(',').map(function (a) {
                    return parseFloat(a);
                }).reverse()
            };
            locations[csvArr[0]] = loc;
        })
        .on('done',(error)=> {
            console.log("end of location stream");

            var bulk = [];
            csv({
                delimiter: '\t',
                noheader: true,
                quote: false,
                checkType:false
            })
                .fromFile('t/twitter_cikm_2010/test_set_tweets.txt')
                .on('csv',(tCsv, tIndex)=>{
                    if (tCsv.length !== 4) {
                        return;
                    }
                    let jsonObj = {
                        'userId' : tCsv[0],
                        'tweetId' : tCsv[1],
                        'text' : tCsv[2],
                        'date' : tCsv[3],
                        'location' : locations[tCsv[0]]
                    };
                    bulk.push({
                        insertOne: {
                            document : jsonObj
                        }
                    });
                    if (bulk.length === 100000) {
                        collection
                            .bulkWrite(
                                bulk,
                                {ordered:false},
                                function (err, r) {
                                    console.log("inserted batch " + tIndex);
                                }
                            );
                        bulk = [];
                    }
                })
                .on('done',(error)=>{
                    console.log("end of stream");
                    if (bulk.length !== 0) {
                        collection
                            .bulkWrite(
                                bulk,
                                {ordered:false},
                                function (err, r) {
                                    console.log("completed ");
                                }
                            );
                        bulk = [];
                    }
                })
        });
}

function import_locations(collection) {
}

router.get('/tweets', function (req, res, next) {
    res.send('importing tweets at background');

    connect_db(import_tweets);
});

router.get('/locations', function (req, res, next) {
    res.send('importing location at background');

    connect_db(import_locations);
});

module.exports = router;