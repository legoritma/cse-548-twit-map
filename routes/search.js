var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;

function connect_db(req, res, callback) {
    MongoClient.connect(
        'mongodb://127.0.0.1:27017/',
        function (err, dbServer) {
            if (err) {
                console.log(err);
                return;
            }
            var db = dbServer.db("twitter_map");

            db.createCollection("tweets",
                function (err, results) {
                    if (err) {
                        console.log(err);
                        return;
                    }
                    console.log('collection created');

                    var collection = db.collection('tweets');

                    var promises = [];
                    promises.push(collection.createIndex(
                        {location: '2dsphere'}
                    ));
                    promises.push(collection.createIndex(
                        {tweetId: 1},
                        {unique: true}
                    ));
                    promises.push(collection.createIndex(
                        {text: 'text'}
                    ));
                    Promise.all(
                        promises
                    ).then(function (values) {
                        console.log("indexes created");
                        callback(req, res, collection);
                    })
                    .catch(function (e) {
                        console.log(e)
                    })
                }
            );
        }
    );
}

function search_tweets(req, res, collection) {
    console.log(req.body);
    let ne_lat = parseFloat(req.body.ne_lat);
    let ne_lng = parseFloat(req.body.ne_lng);
    let sw_lat = parseFloat(req.body.sw_lat);
    let sw_lng = parseFloat(req.body.sw_lng);
    let start = 0;
    let limit = 10;
    if (req.body.start) {
        start = parseInt(req.body.start);
    }
    if (req.body.limit) {
        limit = parseInt(req.body.limit);
    }
    if (!req.body.keyword || req.body.keyword.length === 0) {
        res.send(JSON.stringify({error: 'keyword is required'}));
        return;
    }
    let query = {
        '$text': {'$search': req.body.keyword, '$caseSensitive' : false },
        'location': {
            $geoWithin: {
                $geometry: {
                    type: "Polygon",
                    coordinates: [[
                        [ne_lng, ne_lat],
                        [sw_lng, ne_lat],
                        [sw_lng, sw_lat],
                        [ne_lng, sw_lat],
                        [ne_lng, ne_lat]
                    ]]
                }
            }
        }
    };
    let options = {
        "limit": limit,
        "skip": start,
        "sort": "date",
    };
    collection
        .find(query, options)
        .toArray(function (err, docs) {
            if (err) {
                res.send(JSON.stringify({error: err.message}));
                return;
            }

            console.log("Found the following records " + docs.length);
            let response = {
                limit: limit,
                start: start,
                tweets: docs
            };
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(response));
        });
}

router.post('/', function (req, res, next) {
    connect_db(req, res, search_tweets);
});

module.exports = router;